export interface InlineBadge {
  cssClass: string;
  label?: string;
  labelParams?: {};
}
