
export interface BackendVersion {
  build: {
    version: string
  };
}
