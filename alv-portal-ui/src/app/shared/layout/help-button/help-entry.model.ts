export interface HelpEntry {
  title: string;
  text?: string;
  listItems?: Array<string>;
}

