export enum InputType {
  INPUT_FIELD = 'input-field',
  DATE_INPUT = 'date-input',
  CHECKBOX = 'checkbox',
  RADIO_BUTTON = 'radio-button',
  SELECT = 'select',
  SINGLE_TYPEAHEAD = 'single-typeahead',
  MULTI_TYPEAHEAD = 'multi-typeahead'
}
